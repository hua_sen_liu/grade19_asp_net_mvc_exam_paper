﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamDemo.ParamModel
{
    public class RegisterViewData
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Confirmpassword { get; set; }
    }
}