﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamDemo.ParamModel
{
    public class LoginViewData
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}