﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamDemo.ViewModel
{
    public class CommentViewModel
    {
        public string Content { get; set; }
        public string FromUserName { get; set; }
        public string ToUserName { get; set; }
    }
}