﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamDemo.ViewModel
{
    public class UserViewModel
    {
        public string UserName { get; set; }
        public IEnumerable<MessageViewModel> MessageViewModels { get; set; }
    }
}