﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ExamDemo.Data;
using ExamDemo.Models;
using ExamDemo.ParamModel;
using Newtonsoft.Json;

namespace ExamDemo.Controllers
{
    public class UsersController : Controller
    {
        private ExamDemoDB db = new ExamDemoDB();


        // 登录
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public string LoginDone(LoginViewData loginViewData)
        {
            dynamic result;
            var userName = loginViewData.Username.Trim();
            var password = loginViewData.Password.Trim();

            if (userName.Length > 0 && password.Length > 0)
            {
                var userLogin = db.Users.Where(x => x.UserName == userName && x.Password == password).FirstOrDefault();
                if (userLogin != null)
                {
                    result = new
                    {
                        code = 200,
                        msg = "登陆成功"
                    };
                }
                else
                {
                    // 账号密码错误
                    result = new
                    {
                        code = 403,
                        msg = "账号密码错误"
                    };
                }
            }
            else
            {
                // 账号密码不能为空
                result = new
                {
                    code = 403,
                    msg = "账号密码不能为空"
                };
            }

            return JsonConvert.SerializeObject(result);
        }


        // 注册
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public string RegisterDone(RegisterViewData registerViewData)
        {
            dynamic result;
            var userName = registerViewData.Username.Trim();
            var password = registerViewData.Password.Trim();
            var confirmpassword = registerViewData.Confirmpassword.Trim();

            if (userName.Length > 0 && password.Length > 0 && confirmpassword == password)
            {
                var multipleUserName = db.Users.Where(x => x.UserName == userName).FirstOrDefault();
                if (multipleUserName == null)
                {
                    db.Users.Add(new Users
                    {
                        UserName = userName,
                        Password = password
                    });
                    db.SaveChanges();

                    result = new
                    {
                        code = 200,
                        msg = "注册成功"
                    };
                }
                else
                {
                    // 用户名已存在
                    result = new
                    {
                        code = 403,
                        msg = "用户名已存在"
                    };
                }

            }
            else
            {
                // 账号密码不能为空，并且密码和确认密码要一致
                result = new
                {
                    code = 403,
                    msg = "账号密码不能为空，并且密码和确认密码要一致"
                };
            }


            return JsonConvert.SerializeObject(result);
        }
































        // GET: Users
        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性；有关
        // 更多详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserName,Password,CreateTime,UpdateTime,Versions,Remark")] Users users)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(users);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(users);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // POST: Users/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性；有关
        // 更多详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserName,Password,CreateTime,UpdateTime,Versions,Remark")] Users users)
        {
            if (ModelState.IsValid)
            {
                db.Entry(users).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(users);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Users users = db.Users.Find(id);
            db.Users.Remove(users);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
