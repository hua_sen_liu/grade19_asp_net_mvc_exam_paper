﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ExamDemo.Data;
using ExamDemo.Models;
using ExamDemo.ViewModel;

namespace ExamDemo.Controllers
{
    public class MessagesController : Controller
    {
        private ExamDemoDB db = new ExamDemoDB();

        // GET: Messages
        public ActionResult Index()
        {

            var users = db.Users.ToList();
            var message = db.Messages.ToList();
            var comment = db.Comments.ToList();

            var msgViewModel = new List<MessageViewModel>();
            foreach (var msg in message)
            {
                var comViewModel = new List<CommentViewModel>();
                var currentAllComment = comment.Where(x=>x.ToMsgId == msg.Id).ToList();

                foreach(var com in currentAllComment)
                {
                    var fromUserName = users.Where(x => x.Id == com.FromUserId).FirstOrDefault().UserName;
                    var commentAMsg = message.Where(x => x.Id == com.ToMsgId).FirstOrDefault();
                    var toUserName = users.Where(x => x.Id == commentAMsg.FromUserId).FirstOrDefault().UserName;

                    comViewModel.Add(new CommentViewModel
                    {
                        Content = com.Contert,
                        FromUserName = fromUserName,
                        ToUserName = toUserName
                    });
                }


                msgViewModel.Add(new MessageViewModel
                {
                    UserId = msg.FromUserId,
                    Content = msg.Contert,
                    CommentViewModels = comViewModel
                });

            }

            var vm = new UserViewModel
            {
                UserName = "欢迎来到德莱联盟",
                MessageViewModels = msgViewModel
            };

            return View(vm);
        }

        // GET: Messages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Messages messages = db.Messages.Find(id);
            if (messages == null)
            {
                return HttpNotFound();
            }
            return View(messages);
        }

        // GET: Messages/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Messages/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性；有关
        // 更多详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Contert,FromUserId,CreateTime,UpdateTime,Versions,Remark")] Messages messages)
        {
            if (ModelState.IsValid)
            {
                db.Messages.Add(messages);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(messages);
        }

        // GET: Messages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Messages messages = db.Messages.Find(id);
            if (messages == null)
            {
                return HttpNotFound();
            }
            return View(messages);
        }

        // POST: Messages/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性；有关
        // 更多详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Contert,FromUserId,CreateTime,UpdateTime,Versions,Remark")] Messages messages)
        {
            if (ModelState.IsValid)
            {
                db.Entry(messages).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(messages);
        }

        // GET: Messages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Messages messages = db.Messages.Find(id);
            if (messages == null)
            {
                return HttpNotFound();
            }
            return View(messages);
        }

        // POST: Messages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Messages messages = db.Messages.Find(id);
            db.Messages.Remove(messages);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
