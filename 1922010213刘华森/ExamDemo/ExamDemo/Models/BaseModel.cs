﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamDemo.Models
{
    public class BaseModel
    {
        public BaseModel()
        {
            CreateTime = DateTime.Now;
            UpdateTime = DateTime.Now;
            Versions = 0;
        }

        public int Id { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdateTime { get; set; }
        public int Versions { get; set; }
        public string Remark { get; set; }
    }
}