﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ExamDemo.Models;

namespace ExamDemo.Data
{
    public class InitializeData : DropCreateDatabaseIfModelChanges<ExamDemoDB>
    {
        protected override void Seed(ExamDemoDB db)
        {
           
            db.Users.AddRange(new List<Users>
            {
                new Users
                {
                    UserName = "李华",
                    Password = "123"
                },
                new Users
                {
                    UserName = "小明",
                    Password = "123"
                },
                new Users
                {
                    UserName = "张三",
                    Password = "123"
                },
            });


            db.Messages.AddRange(new List<Messages>
            {
                new Messages
                {
                    Contert = "蜀道之难",
                    FromUserId = 1
                },
                new Messages
                {
                    Contert = "难于上青天",
                    FromUserId = 2
                },
                new Messages
                {
                    Contert = "窗前明月光",
                    FromUserId = 3
                },
            });

            db.Comments.AddRange(new List<Comments>
            {
                new Comments
                {
                    Contert = "疑是地上霜",
                    FromUserId = 2,
                    ToMsgId = 1
                },
                new Comments
                {
                    Contert = "举头望明月",
                    FromUserId = 1,
                    ToMsgId = 2
                },
                new Comments
                {
                    Contert = "低头思故乡",
                    FromUserId = 3,
                    ToMsgId = 3
                },
            });

            db.SaveChanges();

            base.Seed(db);
        }
    }
}